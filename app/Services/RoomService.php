<?php

namespace App\Services;

use DB;
use App\Exceptions\SaveToDatabaseException;


class RoomService
{
    /**
     * @param $uuid
     * @return mixed
     * @throws SaveToDatabaseException
     */
    public function getRoomEntries($uuid)
    {

        try {

            $entries = DB::table('entry')
                ->join('room', 'room.roomid', '=', 'entry.room_roomid')
                ->where('room.uuid', '=', $uuid)
                ->orderBy('position', 'asc')
                ->get();

            return $entries;

        } catch(\Exception $e) {
            DB::rollback();
            throw new SaveToDatabaseException($e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function createNewRoom()
    {

        $newUuid = $this->generate_uuid();

        DB::table('room')->insert(
            array(
                'uuid' => $newUuid)
        );

        return $newUuid;
    }

    /**
     * @param $uuid
     * @return bool
     */
    public function clearRoom($uuid)
    {

        // delete everything in the room
        DB::table('entry')
            ->join('room', 'entry.room_roomid', '=', 'room.roomid')
            ->where('room.uuid', '=', $uuid)
            ->delete();

        return true;
    }

    /**
     * @return string
     */
    private function generate_uuid()
    {

        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );

    }


}