<?php

namespace App\Services;

use DB;
use App\Exceptions\SaveToDatabaseException;


class EntryService
{

    /**
     * @param $content
     * @param $roomUuid
     * @return mixed
     */
    public function addEntryToRoom($content, $roomUuid)
    {

        // get room information
        $roomInfo = DB::table('room AS r')
            ->leftJoin('entry AS e', function($join) use($roomUuid)
            {
                $join->on('r.roomid', '=', 'e.room_roomid');
            })
            ->select( DB::raw(" IF( MAX(position) IS NOT NULL, MAX(position), '0' ) as lastPosition"), 'r.roomid')

            ->where('r.uuid', '=', $roomUuid)
            ->first();

        // insert the entry
        DB::table('entry')->insert(
            array(
            'content' => $content,
            'position' => $roomInfo->lastPosition + 1,
            'room_roomid' => $roomInfo->roomid)
        );

        return true;
    }



}