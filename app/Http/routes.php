<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'DashboardController@handleGet')->name('dashboard');
Route::get('/dashboard', 'DashboardController@handleGet')->name('dashboard');

Route::get('/room/{uuid?}', 'RoomController@handleGet')->name('room.get');




Route::post('/save/{uuid}', 'RoomController@saveRoom')->name('room.save');

Route::group(['middleware' => ['ajax']], function () {

    //Route::get('/checkForUpdate/{uuid?}', 'RoomController@handleGet')->name('room.check');

});
