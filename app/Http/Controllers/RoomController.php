<?php
/**
 * Created by PhpStorm.
 * User: andrew.gosali
 * Date: 8/08/2016
 * Time: 11:27 AM
 */

namespace App\Http\Controllers;

use App\Services\RoomService;
use App\Services\EntryService;
use Illuminate\Http\Request;


class RoomController extends Controller
{

    /**
     * @param Request $request
     * @param null $uuid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\SaveToDatabaseException
     */
    public function handleGet(Request $request, $uuid = null)
    {

        $entries = array();

        $roomService = new RoomService();

        //if not in the path, get from the GET request
        if (empty($uuid)) {

            $uuid = $request->input('uuid');

        }

        if (empty($uuid)) {

            // create new room
            $uuid = $roomService->createNewRoom();

        } else {

            // load existing room
            $entries = $roomService->getRoomEntries($uuid);

        }


        return view('room.room', [
            'title' => 'Room',
            'navigationOptions' => $this->navigationOptions,
            'bodyClass' => 'room',
            'uuid' => $uuid,
            'entries' => $entries

        ]);

    }

    /**
     * @param Request $request
     * @param $uuid
     * @return bool
     */
    public function saveRoom(Request $request, $uuid)
    {

        $roomService = new RoomService();

        $entryService = new EntryService();

        $roomService->clearRoom($uuid);

        $requestData = $request->all();

        $entries = json_decode($requestData['entries_json']);

        foreach ($entries as $content) {

            $entryService->addEntryToRoom($content, $uuid);

        }

        return response()->json([
            'html' => 'success'
        ]);

    }
}