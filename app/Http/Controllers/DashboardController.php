<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handleGet(Request $request)
    {
        return view('dashboard', [
            'title' => 'Dashboard',
            'navigationOptions' => $this->navigationOptions,
            'bodyClass' => 'dashboard'
        ]);
    }

}
