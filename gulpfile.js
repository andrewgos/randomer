var elixir = require('laravel-elixir');

var paths = {
    nodeModulesPath: '../../../node_modules/'
}

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/resources/css/app.css') // compile the app sass file
        .styles([ // combine all the vendor styles into a single "vendors.css" in the public directory
            paths.nodeModulesPath + "/bootstrap/dist/css/bootstrap.css",
            paths.nodeModulesPath + "/bootstrap/dist/css/bootstrap.css.map",
            paths.nodeModulesPath + "/font-awesome/css/font-awesome.min.css",
            paths.nodeModulesPath + "/font-awesome/css/font-awesome.css.map"
        ], 'public/resources/css/vendors.css')
        .scripts([ // combine all the vendor js files into a "vendors.js" in the public directory
            paths.nodeModulesPath + "jquery/dist/jquery.min.js",
            paths.nodeModulesPath + "bootstrap/dist/js/bootstrap.min.js"
        ], 'public/resources/js/vendors.js')
        .scripts([
            'room.js'
        ], 'public/resources/js/app.js')
        .copy('node_modules/bootstrap/dist/fonts', 'public/resources/fonts')
        .copy('node_modules/font-awesome/fonts', 'public/resources/fonts');
});
