<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Randomer - Group Randomizer</title>

        <!-- Vendor Styles -->
        <link href="{{ asset('resources/css/vendors.css') }}" rel="stylesheet">
        <!-- App Styles -->
        <link href="{{ asset('resources/css/app.css') }}" rel="stylesheet">

    </head>

    <body class="{{ $bodyClass }}">
        <div class="container">
            <nav class="navbar navbar-default">

                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand logo" href="{{ route('dashboard') }}">
                            {{--<img alt="Brand" src={{asset('resources/images/TOTO_BETA-01.png')}} width="80">--}}
                            <strong>Randomer</strong>
                        </a>

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                        <ul class="nav navbar-nav">
                            <li class=""><a href="{{ route('dashboard') }}">Dashboard</a></li>
                            <li class=""><a href="">Create New</a></li>
                        </ul>

                    </div>
                </div>

            </nav>


        </div>

        @yield('content')

    </body>


    <script src="{{ asset('resources/js/vendors.js') }}"></script>

    @yield('scripts')

</html>