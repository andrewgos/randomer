@extends('layouts.default')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="page-header col-sm-offset-3">
        <h2>Your Room ID: <strong><span id="uuid">{{ $uuid  }}</span></strong></h2>
    </div>

    <div class="panel-body col-sm-offset-4">
        <!-- Display Validation Errors -->
        {{--@include('common.errors')--}}

       {{ csrf_field() }}

        <!-- Add Item Button -->
        <div class="container-fluid row">
            <div class="col-sm-3">
                <input type="text" name="name" class="form-control" id="input-content">
            </div>
            <div class="col-sm-1">
                <button type="submit" class="btn btn-default" id="add-item-btn">
                    <i class="fa fa-plus"></i> Add Item
                </button>
            </div>
            <div class="col-sm-1">
                <button type="submit" class="btn btn-default" id="randomize-btn">
                    <i class="fa fa-plus"></i> Randomize
                </button>
            </div>




        </div>



        &nbsp;

        <div class="container-fluid row">
            <div class="col-sm-3">
                <ul class="list-group text-center" id="content-list">
                    @foreach($entries as $entry)
                        <li class="list-group-item content">{{ $entry->content }}</li>
                    @endforeach
                </ul>
            </div>
        </div>





    </div>

@endsection

@section('scripts')
    <script src="{{ asset('resources/js/app.js') }}"></script>
@stop