@extends('layouts.default')

@section('content')
    <div class="col-xs-12 col-md-6 col-md-offset-4">

        <div class="row mt-10p">
            <div class="col-xs-12 col-md-6">
                <div class="thumbnail">
                    <div class="caption">
                        <p>Create your own room</p>
                        <a href="/room" rel="button" type="button" class="btn btn-info btn-block btn-lg">
                            New Room
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-10p">
            <div class="col-xs-12 col-md-6">
                <div class="thumbnail">
                    <div class="caption">

                        <p>Open an existing room</p>
                        <form action="/room/" method="GET">
                            <input type="text" class="form-control" name="uuid">
                            &nbsp;
                            <button rel="button" type="submit" class="btn btn-info btn-block btn-lg">
                                Load Room
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop