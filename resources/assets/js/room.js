;(function(window, console,$){

    $(document).ready(function() {

        if(!$('body').hasClass('room')) { // check if this script needs to be run for this page
            return;
        }

        $('#add-item-btn').on('click', function(){

            var newContent = $('#input-content').val();

            $('#content-list').append("<li class='list-group-item content'>" + newContent + "</li>");

            $('#input-content').val('');

            saveRoom();

        });

        $('#randomize-btn').on('click', function(){

            $('#content-list').randomize('li');

            saveRoom();

        });

        //TODO: call to check update on the page



    });

    function saveRoom() {

        // get the list of entries
        var entries = $(".content").map(function() {
            return $(this).text()
        }).get();

        // prepare data to be submitted through ajax
        var formData = new FormData();
        formData.append ('entries_json', JSON.stringify(entries));

        // save the contents
        var url = '/save/7788796b-d965-4669-b353-52d9b7ca78ca';
        $.ajax({
            data: formData,
            method: 'POST',
            url: url,
            dataType: 'json',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('[name=_token]').val()
            }
        });
    }

    $.fn.randomize = function(selector){
        var $elems = selector ? $(this).find(selector) : $(this).children(),
            $parents = $elems.parent();

        $parents.each(function(){
            $(this).children(selector).sort(function(){
                return Math.round(Math.random()) - 0.5;
                // }). remove().appendTo(this); // 2014-05-24: Removed `random` but leaving for reference. See notes under 'ANOTHER EDIT'
            }).detach().appendTo(this);
        });

        return this;
    };

}(window, console, window.jQuery));
